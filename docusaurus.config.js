// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Constella',
  tagline: 'Go live in minutes',
  url: 'https://idti-widgets.gitlab.io/',
  baseUrl: '/widgets-showcase/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/constella-32x32.png',
  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'idti-widgets', // Usually your GitHub org/user name.
  projectName: 'widgets-showcase', // Usually your repo name.

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },
  scripts : [
  ],

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'Constella',
        logo: {
          alt: 'Widgets',
          src: 'img/constella-32x32.png',
        },
        items: [
          {
            type: 'doc',
            docId: 'widgets',
            position: 'left',
            label: 'Documentation',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'Tutorial',
                to: '/docs/widgets',
              },
            ],
          },
          {
            title: 'MORE INFORMATION',
            items: [
              {
                label: 'Who We Are',
                href: 'https://constellaintelligence.com/who-we-are/',
              },
              {
                label: 'Blog',
                href: 'https://constellaintelligence.com/threat-intelligence-blogs/',
              },
              {
                label: 'Contact Us',
                href: 'https://constellaintelligence.com/contact-us/',
              },
              {
                label: 'Request a Demo',
                href: 'https://info.constellaintelligence.com/request-a-demo',
              },
              {
                label: "Support",
                href: 'https://constellaintelligence.com/support/'
              }
            ],
          },
          {
            title: 'Media',
            items: [
              {
                label: 'Twitter',
                to: 'https://www.twitter.com/ConstellaIntel',
              },
              {
                label: 'Linkedin',
                href: 'https://www.linkedin.com/company/constella',
              },
              {
                label: "Blog",
                href: 'https://constellaintelligence.com/feed/atom/?category_name=Blog'
              }
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Constella Intelligence. All rights reserved.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
