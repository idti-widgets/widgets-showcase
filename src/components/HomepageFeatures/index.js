import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Go Live in Days',
    Svg: require('@site/static/img/go-live-in-days.svg').default,
    description: (
      <>
        Intuitive components that can be integrated into flow with a few lines of code.
      </>
    ),
  },
  {
    title: 'Choose who gets the tools',
    Svg: require('@site/static/img/choose-what-gets-done.svg').default,
    description: (
      <>
        Use highly customizable targeting logic to surface relevant offers for each customer.
      </>
    ),
  },
  {
    title: 'Powered by Constella',
    Svg: require('@site/static/img/fraud-Protection.svg').default,
    description: (
      <>
        Get real-time, contextual intelligence to stay ahead of a rapidly changing cyber threat environment, empowering you to make fast, informed decisions.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
