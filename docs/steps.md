---
sidebar_position: 1
---


# Setup
- Configure token in tag [docs](./backend)
- Add base libraries [docs](./base)
- Select components [docs](./widgets) 