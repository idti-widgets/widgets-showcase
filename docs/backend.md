---
sidebar_position: 2
---

# Backend


## Partner User's identity or authentication 

As part of onboarding partner will be provided with
  1. Client_id
  2. Client_Secret

Once User logs in to partner's website, it should compute a token `X-WIDGETS-JWT` via passing below payload to constella services.


<iframe src="//api.apiembed.com/?source=https://cedric05.github.io/sample-json/payload.json&targets=node:unirest,java:unirest,python:requests,php:curl,ruby:native,go:native,shell:curl" frameborder="0" scrolling="no" width="100%" height="500px" seamless></iframe>


### Request payload
```json
{
  "client_id": "<client_id>",
  "client_secret": "<client_secret>",
  "user_id": "<username>", // Partner user identity (should be unique across partner's users)
  "age": 30000 // expire time
}
```


Successfull response is like blow
### Request response

```json
{
	"refresh": "xxxx",
	"access": "xxxxx" // X-WIDGETS-JWT
}

```

X-Widgets-token can be should be created once per partner's user session. To render frontend items use below 

```html
  <v-app>
    <idti-emails usertoken="<X-WIDGETS-JWT from above request>"></idti-emails>
  </v-app>
```


Partner can also set above `X-WIDGETS-JWT` token via cookie also but we suggest you go with above approach. Constella IDTI-Widgets identifies users via above usertoken/cookie and responds appropriately 