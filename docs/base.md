---
sidebar_position: 3
---
# Base Libraries
Add these libraries to web page. 
```html
    <link rel="stylesheet" href="http://sdk.constellaintelligence.com/idti-widgets.css">
    <script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>
    <script src="http://sdk.constellaintelligence.com/idti-widgets.umd.min.js"></script> 
```