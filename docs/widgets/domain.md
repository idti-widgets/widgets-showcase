---
sidebar_position: 3
---

# Domain Widget


Provides a way for end users to register their domains for alert monitoring

```html
  <v-app>
    <idti-domains></idti-domains>
  </v-app>
```


![Domains](/img/domains.png)