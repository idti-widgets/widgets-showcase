---
sidebar_position: 7
---

# SSN Widget


Registration and list ssns widget

```html
  <v-app>
    <idti-ssns></idti-ssns>
  </v-app>
```

## Example
![SSNS](/img/ssn.png)
