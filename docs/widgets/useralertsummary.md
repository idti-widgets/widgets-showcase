---
sidebar_position: 10
---

# User Alert Summary


Summary of all the alerts recieved for an user

```html
  <v-app>
    <idti-user-alert-summary></idti-user-alert-summary>
  </v-app>
```


![Alert Summary](/img/user-alert-summary.png)