---
sidebar_position: 2
---

# Email Widget


## Email Registration

Provides a way for end users to register their personal emails for alert monitoring
```html
  <v-app>
    <idti-emails></idti-emails>
  </v-app>
```

![Emails](/img/emails.png)
