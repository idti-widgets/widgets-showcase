---
sidebar_position: 4
---

# Alert Summary


Summary of all the alerts recieved for an user

```html
  <v-app>
    <idti-alerts-summary></idti-alerts-summary>
  </v-app>
```


![Alert Summary](/img/summary.png)