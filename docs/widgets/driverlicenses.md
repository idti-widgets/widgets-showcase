---
sidebar_position: 7
---

# Driver License Widget


Registration and list driverlicenses widget

```html
  <v-app>
    <idti-driverlicenses></idti-driverlicenses>
  </v-app>
```

## Example
![Driver License](/img/driverlicense.png)