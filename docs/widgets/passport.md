---
sidebar_position: 6
---

# Passport Widget


Registration and list Passport widget

```html
  <v-app>
    <idti-passports></idti-passports>
  </v-app>
```

**Sample Monitor Passport**: 


![Monitor Passport](/img/passport.png)