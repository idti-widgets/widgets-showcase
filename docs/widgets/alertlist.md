---
sidebar_position: 5
---

# Alert List Widget

List of all the alerts recieved for an user


```html
  <v-app>
    <idti-alerts-list></idti-alerts-list>
  </v-app>
```

**Sample Alert List**: 

![Alert List](/img/alertlist.png)

**Sample Alert Details**: 

![Alert Details](/img/alert_details.png)
