---
sidebar_position: 7
---

# Telephone Widget


Registration and list Telephone numbers widget

```html
  <v-app>
    <idti-telephones></idti-telephones>
  </v-app>
```


## Example
![Telephone](/img/telephone.png)