---
sidebar_position: 5
---

# Architecture

Constella's IDTI-widgets gives you feasibility to add awesome widgets without any hassle of integration. Constella IDTI-widgets takes care of complete user management, alerts, data from breach management.


IDTI widgets works using below components
1. IDTI frontend widget library
   > Wide range of UI library to integrate entriprise website with just adding html components
2. IDTI backend
   > Handles all callback from constella idti
   > aggregates and alerts partner's user
3. Postgres
   > For saving alerts from idti, userdata and its management
4. UI Gateway
   > For creating new partner and its management


![Architecture](/img/architecture.png)



Constella manages above entire infrastructure so, that partners/client can just integrate and work on their main focus area.


## UI Gateway

UI Gateway is single point of contact for entire partner management. It takes ownership of provisioning new partner, partner documentation and its complete management. 

It will also provide a unique username, token, app_token by using this it will authenticate & autherize the API requests and provide the data.


## Widgets backend for Internal Authentication and Storing the Alerts

As part of provisioning, widgets backend will need username, token, app_token provided by IDTI / UI Gateway and generates a client_id & client_secret.

1. `client_id` unique for client
2. `client_secret` is secret and should not be shared and can be reset.

`client_id` and `client_secret` are mainly used for authenticating partner website users. Widgets backend will make IDTI calls with respective client/partners tokens to register the PII information & get the alerts.

Note: For now, Widgets backend will be common for all clients/partners which is managed by Constella.


### Authentication

Partner's users authentication follows below process

1. Partner users uses his/her credentials to login to partner website.
2. Partner authenticates user credentials and rediects to partner website
   1. Before redirection, Partner also generates `x-widgets-token` (unique for each user and each login) after partner's user session is created
3. Widgets-Backend library reads this embeded `x-widgets-token` and authenticate & autherize the users
4. Once the user is authenticated it will use respective client/partners IDTI tokens to make requests to IDTI
