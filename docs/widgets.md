---
sidebar_position: 4
---

# Available Widgets

- [Email Widget](./widgets/emails)
- [Domain Widget](./widgets/domain)
- [Alert Summary](./widgets/alertsummary)
- [Alert List](./widgets/alertlist)
- [Credit Card](./widgets/creditcard)
- [Driver license](./widgets/driverlicenses)
- [Passport](./widgets/passport)
- [ssns](./widgets/ssns)
- [Telephone/Mobile](./widgets/telephones)
